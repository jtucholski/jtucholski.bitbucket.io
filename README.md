# Contributing

To contribute to this site, create a branch `git branch <branch-name>` to make your changes.

Once the branch is created check it out using `git checkout <branch-name>`.

You won't have direct push access to `master`. Therefore you'll need to push to your branch and submit a pull request.